package pruebas;

public class MatrixCD
{
    public static void main(String[] args) {
        double[][] matriz = {{1, 0, 2}, {2, 1, 1}, {1, 1, 1}};
        System.out.println(MatrixCD.det(matriz));
    }

    public static double[][] intercambiarFilasMatriz(double[][] matriz, int i, int j)
    {
        double temp;
        for (int k = 0; k < matriz[0].length; k++)
        {
            temp = matriz[i][k];
            matriz[i][k] = matriz[j][k];
            matriz[j][k] = temp;
        }
        return matriz;
    }

    public static void imprimirMatriz(double[][] matriz)
    {
        for (int i = 0; i < matriz.length; i++)
        {
            for (int j = 0; j < matriz[0].length; j++)
            {
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public static int pivoteMatriz(double[][] matriz, int i)
    {
        int piv = i;
        double v = matriz[i][i];
        for (int j = i + 1; j < matriz.length; j++)
        {
            if (Math.abs(matriz[j][i]) > v)
            {
                v = matriz[j][i];
                piv = j;
            }
        }
        return piv;
    }

    /**
     * 
     * @param m
     *            es una matriz cuadrada
     * @return
     */
    public static int eliminaAdelante(double[][] m)
    {
        int numeroIntercambios = 0;
        int filaPivote = -1;
        for (int columnaEvaluada = 0; columnaEvaluada < m.length; columnaEvaluada++)
        {
            filaPivote = pivoteMatriz(m, columnaEvaluada);
            if (filaPivote != columnaEvaluada)
            {
                m = intercambiarFilasMatriz(m, columnaEvaluada, filaPivote);
                numeroIntercambios++;
            }
            for (int i = columnaEvaluada + 1; i < m.length; i++)
            {
                for (int j = columnaEvaluada + 1; j < m.length; j++)
                {
                    if (m[columnaEvaluada][columnaEvaluada] == 0)
                    {
                        System.out.println("Error, divisi�n por cero");
                        System.exit(1);
                    } else
                    {
                        m[i][j] = m[i][j]
                                - (m[i][columnaEvaluada] * m[columnaEvaluada][j] / m[columnaEvaluada][columnaEvaluada]);
                    }
                }
                m[i][columnaEvaluada] = 0;
            }
        }
        return numeroIntercambios;
    }

    public static double det(double[][] m)
    {
        int numeroIntercambios = MatrixCD.eliminaAdelante(m);
        double det = 1;
        for (int i = 0; i < m.length; i++)
        {
            det *= m[i][i];
        }
        if (numeroIntercambios % 2 == 1)
        {
            det = -det;
        }
        return det;
    }
}
