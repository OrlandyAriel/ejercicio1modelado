package pruebas;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.DigestException;
import ull.modelado.excepciones.DimesionException;
/**
 * @author Orlandy Ariel S�nchez A.
 */
public class Matrix
{   // Atributos
	private int m_filas, m_columnas;
	private Fraction[][] m_matrix;
	//private ArrayList<ArrayList<Integer>> m_matrix;
	// Constructor/es
	public Matrix()
	{
		this(1, 1);
	}
	public Matrix(int m, int n)
	{
		this(m, n, new Fraction[m][n]);
	}
	public Matrix(int m, int n, Fraction [][]a_coef)
	{
		m_filas = m;
		m_columnas = n;
		if((m == a_coef.length) && (n == a_coef[0].length) )
		{
			m_matrix = a_coef;
		}
		else {
			try
			{
				throw new DigestException("Matriz de distinta a las dimensiones de la matriz pasada.");
			} catch (DigestException e)
			{
				e.printStackTrace();
			}
		}
	}
	// M�todos
	/**
	 * A�ade elementos a la matriz en la posici�n que se indique por parametros
	 * @param a_m Fila
	 * @param a_n Columna
	 * @param a_num Elemento a introducir
	 */
	public void anadir(int a_m,int a_n, Fraction  a_num)
	{
	    if(a_m < m_filas && a_n < m_columnas)
	    {
	        m_matrix[a_m][a_n] = a_num;
	    }
	    else
	    {
	        try
            {
                throw new DimesionException("La posici�n a la que se intenta acceder esta fuera del rango");
            } catch (DimesionException e)
            {
                e.printStackTrace();
            }
	    }
	}
	/**
	 * M�todo que calcula el producto entre dos matrices.
	 * @param a_matrix 
	 * @return Devuelve una matriz nueva con el resultado del producto
	 * @throws DimesionException 
	 */
	public Matrix producto(Matrix a_matrix) throws DimesionException
	{
		Fraction [][] t_mat_aux = new Fraction [m_filas][a_matrix.getNumCol()];
		int t_col = a_matrix.getNumCol();
		if(m_columnas == a_matrix.m_filas)
		{
			for (int i = 0; i < m_filas; i++)
			{
				for (int j = 0; j < t_col; j++)
				{
				    t_mat_aux[i][j] = new Fraction();
					for (int x = 0; x < m_columnas; x++)
					{
						t_mat_aux[i][j].add( m_matrix[i][x].multiply(a_matrix.m_matrix[x][j]));
					}
				}
			}
		}else
			throw new DimesionException("El n� de columnas de la primera matriz no coincide con el n� de filas de la segunda");
		return new Matrix(m_filas,a_matrix.getNumCol(),t_mat_aux);
	}
	public Fraction getElemento(int a_m, int a_n)
	{
	    return m_matrix[a_m][a_n];
	}
	/**
	 * M�todo que lee de un fichero de texto, pasado como argumento,
	 * el contenido de una matriz.
	 * @param a_nomFichero
	 */
	public static Matrix read(String a_nomFichero)
	{
	    Matrix aux = new Matrix();
	    
		try
		{
			FileReader t_fReader = new FileReader(new File(a_nomFichero));
			BufferedReader t_bReader = new BufferedReader(t_fReader);
			
			aux.m_filas = Integer.parseInt(t_bReader.readLine());
			aux.m_columnas = Integer.parseInt(t_bReader.readLine());
			aux.m_matrix = new Fraction [aux.m_filas][aux.m_columnas];
			String auxString[] = null;
			for (int i = 0; i < aux.m_filas; i++)
			{
			    auxString = t_bReader.readLine().split(" ");
			    for (int j = 0; j < aux.m_columnas; j++)
                {
                    aux.anadir(i, j, new Fraction(auxString[j]));
                }
			    
				//aux.m_matrix[i] = converStringToArrayInt(auxString.split(" "));//convierte el array string a array de int
			}
			t_bReader.close();
			t_fReader.close();
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (NumberFormatException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		return aux;
	}
	/**
	 * M�tod que escribe en un fichero de texto, pasado como argumento,
	 * el contenido de la matriz.
	 * @param a_nomFichero
	 */
	public void write(String a_nomFichero)
	{
        try
        {
            FileWriter t_fw = new FileWriter(new File(a_nomFichero));
            PrintWriter t_pw = new PrintWriter(t_fw);
            t_pw.println(m_filas);
            t_pw.println(m_columnas);
            for (int i = 0; i < m_filas; i++)
            {
                String auxString = "";
                for (int j = 0; j < m_columnas; j++)
                {
                   auxString+= m_matrix[i][j] +" ";
                }
                t_pw.println(auxString);
            }
            t_pw.close();
            t_fw.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
	}
	public Matrix add(Matrix a_mat)
	{
	    Matrix t_aux = new Matrix(a_mat.m_filas, m_columnas);
	    if(this.m_columnas != a_mat.m_columnas || this.m_filas != a_mat.m_filas)
	    {
	        try
            {
                throw new DimesionException("Matrices de distinto tama�o, no se puede realizar la operaci�n");
            } catch (DimesionException e)
            {
                e.printStackTrace();
            }
	    }else
	    {
	       
	        for(int i = 0; i < this.m_filas; i++)
	        {
	            for (int j = 0; j < this.m_columnas; j++)
                {
                    t_aux.anadir(i, j, this.m_matrix[i][j].add( a_mat.m_matrix[i][j]));
                }
	        }
	    }
	    return t_aux;
	}
	/**
	 * M�todo que recibe un array de enteros y retorna un 
	 * String con los elementos del array pasado como argumento
	 * @param a_array
	 * @return El String que contiene los elementos del array separados por un espacio
	 */
	private String convertoArrayIntToString(int [] a_array)
	{
		String t_resul = "";
		for (int i = 0; i < a_array.length; i++)
		{
			t_resul+= a_array[i] + " ";
		}
		return t_resul;
	}
	/**
	 * M�todo que recibe un Array de Strings y lo convierte en un
	 * Array de enteros con los elementos del array pasado como argumento
	 * @param a_array
	 * @return retorna un array de enteros con los elementos pasados a enteros
	 */
	private static int [] converStringToArrayInt(String [] a_array)
	{
		int [] t_resul = new int [a_array.length];
		
		for (int i = 0; i < t_resul.length; i++)
		{
			t_resul[i] = Integer.parseInt(a_array[i]);
		}
		return t_resul;
	}
	
	////########
	public Matrix traspuesta()
	{
	    Fraction [][] t_aux = new Fraction[m_columnas][m_filas];
	    for (int i = 0; i < m_filas; i++)
        {
	        for (int j = 0; j < m_columnas; j++)
            {
                t_aux[j][i] = this.m_matrix[i][j];
            }
        }
	    m_matrix = t_aux;
	    return this;
	}
	//##########
	@Override
	public String toString()
	{
		String t_resul = "";
		for (int i = 0; i < m_filas; i++)
		{
			for (int j = 0; j < m_columnas; j++)
			{
				t_resul += " " + m_matrix[i][j];
			}
			t_resul += "\n";
		}
		return t_resul;
	}
	@Override
	public boolean equals(Object a_obj)
	{
		Matrix t_aux = (Matrix) a_obj;
		if (getNumRow() == t_aux.getNumRow() && getNumCol() == t_aux.getNumCol())
		{
			for (int i = 0; i < m_filas; i++)
			{
				for (int j = 0; j < m_columnas; j++)
				{
					if(m_matrix[i][j]!=t_aux.m_matrix[i][j])
					{
						return false;
					}
				}
			}
		}
		return true;
	}
	public void setM_matrix(Matrix a_matrix)
    {
        this.m_matrix = a_matrix.m_matrix;
    }
	public Fraction[][] getM_matrix()
    {
        return m_matrix;
    }
	
	public int getNumRow()
	{
		return m_filas;
	}

	public int getNumCol()
	{
		return m_columnas;
	}
}
