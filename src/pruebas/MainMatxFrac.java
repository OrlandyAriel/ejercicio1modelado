package pruebas;

import ull.modelado.excepciones.DimesionException;

public class MainMatxFrac
{

    public static void main(String[] args) throws DimesionException
    {
          Fraction [][] a = {{new Fraction(4),new Fraction(4,3)},{new Fraction(3),new Fraction(3)}};
          Fraction [][] b = {{new Fraction(4),new Fraction(4,3)},{new Fraction(3),new Fraction(3)}};
          Matrix aa = new Matrix(2, 2, a);
          Matrix bb = new Matrix(2, 2, b);
          
          Matrix aux = aa.producto(bb);
          System.out.println(aux);
          aux.traspuesta();
          System.out.println("@@@@@@@@@@@@@@");
          System.out.println(aux);
        /*  aux.write("D://a.txt");
          Matrix pruebaLec = Matrix.read("D://a.txt");
          System.err.println(pruebaLec);*/
          
    }

}
