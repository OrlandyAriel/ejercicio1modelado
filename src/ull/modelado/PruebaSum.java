package ull.modelado;

import ull.modelado.ejercicio.Matrix;

public class PruebaSum
{

    public static void main(String[] args)
    {
        Matrix a = new Matrix(2,2);
        Matrix b = new Matrix(2,2);
        a.anadir(0, 0, 3);
        a.anadir(0, 1, 4);
        a.anadir(1, 0, 5);
        a.anadir(1, 1, 6);
        b.anadir(0, 0, 2);
        b.anadir(0, 1, 5);
        b.anadir(1, 0, 1);
        b.anadir(1, 1, 4);
        System.out.println(a);
        System.out.println(b);
        System.err.println(a.add(b));
    }

}
