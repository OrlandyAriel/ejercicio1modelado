package ull.modelado;

import java.util.Scanner;
import ull.modelado.ejercicio.Matrix;
import ull.modelado.ejercicio.SerieFibonacci;
import ull.modelado.ejercicio.Vector;
import ull.modelado.ejercicio.Primos;
import ull.modelado.ejercicio.VectorSinHerencia;
import ull.modelado.excepciones.DimesionException;
/**
 * Pr�ctica 1, Modelado de Software
 * @author Orlandy Ariel S�nchez A.
 */
public class Ejercicio1
{
	public static void main(String[] args) throws DimesionException
    {
	  
       //nPrimos();        
        //fibonacci();
	    vector();
		//matrix();
    }
	@SuppressWarnings("static-access")
    public static void matrix() throws DimesionException
	{	   
	    Matrix t_mat1 = null;
	    Matrix t_mat2 = null;
	    System.out.println("Introduce la dimensi�n de la matriz 1");
	    t_mat1 = rellenar(t_mat1);
	    System.out.println("Introduce la dimensi�n de la matriz 2");
	    t_mat2 = rellenar(t_mat2);
	    System.out.println("Guardar en [f]ichero o mostrar [p]antalla?");
	    Scanner t_sc = new Scanner(System.in);
	    char t_opcion = t_sc.nextLine().charAt(0); 
	    if(t_opcion == 'p')
	        System.out.println(t_mat1.producto(t_mat2));
	    else if(t_opcion == 'f')
	    {
	        System.out.println("Introduce ubucaci�n del fichero donde se guardar�:");
	        Matrix aux = t_mat1.producto(t_mat2);
	        aux.write(t_sc.nextLine());
	    }
	    System.out.println("Introduce la ruta del fichero para leer una matrix:");
	    Matrix t_aux= new Matrix();
	    t_aux.read(t_sc.nextLine());
	    System.out.println("La matriz obtenida:");
	    System.out.println(t_aux);
	    t_sc.close();
	    
	}
	private static Matrix rellenar(Matrix a_mat)
	{
	    Scanner t_sc = new Scanner(System.in);
	    int t_m,t_n;
        System.out.print("Filas:");
        t_m = t_sc.nextInt();
        System.out.print("Columnas:");
        t_n = t_sc.nextInt();
        System.out.println("Introduce los elementos");
        a_mat = new Matrix(t_m, t_n);
        for (int i = 0; i < t_m; i++)
        {
            for (int j = 0; j < t_n; j++)
            {
                System.out.print("elemento["+i+"]["+j+"]:");
                a_mat.anadir(i, j, t_sc.nextInt());
            }
        }
	    return a_mat;
	}
	public static void vector()
	{
	    System.out.println("Introduce la dimensi�n del vector 1:");
	    Scanner t_sc = new Scanner(System.in);
	    Vector t_vec = new Vector(t_sc.nextInt());
	    rellenar(t_vec);
	    System.out.println("Introduce la dimensi�n del vector 2:");
	    Vector t_vec1 = new Vector(t_sc.nextInt());
	    t_vec1 = rellenar(t_vec1);
	    System.out.println("Vector 1: "+t_vec);
        System.out.println("Vector 2:"+t_vec1);
        Vector t_aux = null;
        System.out.println("Producto Escalar:"+t_vec.productoEscalar(t_vec1));
        System.out.println("Introduce la ruta del fichero para guardar el vector resultante:");
        //t_aux.write(t_sc.nextLine());
        t_vec1.write("C://Users/orlan/OneDrive/ULL 2015-2016/Segundo Cuatrimestre/Modelado Software/Pr�cticas/a.txt");
	}
	private static Vector rellenar(Vector a_vec)
	{
	    System.out.println("Introduce los elementos");
	    Scanner t_sc = new Scanner(System.in);
	    for (int i = 0; i < a_vec.getM_dimesion(); i++)
        {
	        System.out.print("Elemento["+i+"]:");
            a_vec.anadir(t_sc.nextInt());
        }
	   
	    return a_vec;
	}
	private static void fibonacci()
    {
	    Scanner t_sc = new Scanner(System.in);
        System.out.print("\nIntrudicir el limite superior para la secuencia:");
        int t_n1 = t_sc.nextInt();
    	SerieFibonacci t_serieFnc= new SerieFibonacci(t_n1);
    	
    	System.out.println(t_serieFnc.toString());
    	System.out.print("Introduce intervalo inferior: ");
    	t_n1=t_sc.nextInt();
    	System.out.print("Introduce intervalo superior: ");
    	int t_n2 =t_sc.nextInt();
    	t_serieFnc.secuencia(t_n1, t_n2);
    	System.out.println(t_serieFnc.toString());   
    	System.out.println("N�mero que desea comprobar si pertence a la secuencia:");
    	t_n2 = t_sc.nextInt();
    	if(t_serieFnc.perteceSecuencia(t_n2))
    		System.out.println("Si pertenece");
    	else
    		System.out.println("No pertenece");
    	t_sc.close();
    }
	private static void nPrimos()
	{
		Primos t_nPrimos =new Primos();
        System.out.println(t_nPrimos.toString());
        
        Scanner t_sc = new Scanner(System.in);
        System.out.println("Suma de los n�meros Primos calculados: "+t_nPrimos.suma());
        System.out.println("\nIntroduce n�mero superior del intervalo:");
        int t_intSuperior= t_sc.nextInt();
        t_sc.close();
        
        t_nPrimos = new Primos(t_intSuperior);
        System.out.println(t_nPrimos.toString());
        System.out.println("Suma de los n�meros Primos calculados: "+t_nPrimos.suma());
       
	}
}
