package ull.modelado.excepciones;

@SuppressWarnings("serial")
public class SecuenciaException extends Exception
{
	public SecuenciaException(String mensaje)
	{
		super(mensaje);
	}
}
