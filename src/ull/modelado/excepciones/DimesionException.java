package ull.modelado.excepciones;

@SuppressWarnings("serial")
public class DimesionException extends Exception
{
	public DimesionException(String mensaje)
	{
		super(mensaje);
	}
}
