package ull.modelado.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;
import ull.modelado.ejercicio.Matrix;
import ull.modelado.excepciones.DimesionException;
/**
 * @author Orlandy Ariel S�nchez A.
 */
public class TestMatrix
{
	@Test
	public void lecturaFichero()
	{
		Matrix t_mat = new Matrix();
		t_mat.read("D://pruebaLectecturaMat.txt");//copiar el archivo pruebaLecturaMat.txt a una direcci�n que prefiera y cambiarlo el argumento
		int [][] t_tabCom = {{56, 77},{16,11}}; 
		Matrix t_compMat = new Matrix(2, 2,t_tabCom);
		assertTrue(t_mat.equals(t_compMat));
	}
	@Test
	public void escrituraFichero()
	{
		int [][] t_tabCom = {{56, 77},{16,11}}; 
		Matrix t_compMat = new Matrix(2, 2,t_tabCom);
		t_compMat.write("D://pruebaEscrituraMat.txt");
		Matrix t_compMat1 = new Matrix();
		t_compMat1.read("D://pruebaEscrituraMat.txt");
		assertTrue(t_compMat.equals(t_compMat1));
	}
	@Test
	public void productoMatrices() throws DimesionException
	{
		int [][] t_tab1 = {{5,3},{1,2},{6,4}};
		int [][] t_tab2 = {{1,3,5},{6,2,3}};
		int [][] t_tabRes = {{23,21,34},{13,7,11},{30,26,42}};
		
		Matrix t_mat1 = new Matrix(3, 2, t_tab1);
		Matrix t_mat2 = new Matrix(2, 3, t_tab2);
		Matrix t_matRes =new Matrix(3,3,t_tabRes);
		
		assertTrue(t_mat1.producto(t_mat2).equals(t_matRes));
	}
	@Test
	public void errorMatrices()
	{
		int [][] t_tab1 = {{5,3},{1,2},{6,4}};
		int [][] t_tab2 = {{1,3},{6,2},{6,4}};
		
		Matrix t_mat1 = new Matrix(3, 2, t_tab1);
		Matrix t_mat2 = new Matrix(3, 2, t_tab2);
		
		try
		{
			t_mat1.producto(t_mat2);
		} catch (DimesionException e)
		{
			fail("El n� de columnas de la primera matriz no coincide con el n� de filas de la segunda");
		}
		
	}
}
