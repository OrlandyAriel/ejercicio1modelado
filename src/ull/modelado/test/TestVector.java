package ull.modelado.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Test;
import ull.modelado.ejercicio.Vector;
import ull.modelado.excepciones.DimesionException;
/**
 * @author Orlandy Ariel S�nchez A.
 */
public class TestVector
{
	@Test
	public void escrbirVectorFichero()
	{
		int [][] t_tabVec = {{2, 3, 5, 1, 6}};
		Vector t_vec = new Vector(t_tabVec,5);
		t_vec.write("D://pruebaLecturaVector.txt");
		assertTrue(t_vec.equals(Vector.read("D://pruebaLecturaVector.txt")));
	}


	@Test
	public void productoES() throws DimesionException
	{
	    int [][] t_tabVec1 = {{1,2,3,3}};
        Vector t_vec1 = new Vector(t_tabVec1,4);
        int [][] t_tabVec2 = {{1,2,3,3}};
        Vector t_vec2= new Vector(t_tabVec2,4);
        assertTrue(t_vec1.productoEscalar(t_vec2) == 23);
	}
	@Test
	public void errorTam()
	{
		int [][] t_tabVec1 = {{2, 3, 5, 1, 6}};
		Vector t_vec1 = new Vector(t_tabVec1,5);
		int [][] t_tabVec2 = {{2, 3, 5, 1}};
		Vector t_vec2 = new Vector(t_tabVec2,4);
		try
		{
			t_vec1.producto(t_vec2);
		} catch (DimesionException e)
		{
			fail("Vectores con distinta dimensi�n");
		}
	}
}
