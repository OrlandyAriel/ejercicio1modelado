package ull.modelado.ejercicio;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import ull.modelado.excepciones.DimesionException;
/**
 * @author Orlandy Ariel S�nchez A.
 */
public class VectorSinHerencia
{ // Atributos
    private int m_dimesion;
    private int[] m_vector;
    private int m_posicion;
    // constructor/es
    public VectorSinHerencia()
    {
        this(1);
    }
    // vector donde se le pasa el tama�o
    public VectorSinHerencia(int a_dimension)
    {
        m_posicion = 0;
        m_vector = new int[a_dimension];
        m_dimesion = a_dimension;
    }
    public VectorSinHerencia(int[]a_vect,int a_dimension)
    {
    	m_dimesion = a_dimension;
    	m_vector = a_vect;
    }
    // funciones
    /**
     *M�todo que recibe el nombre de un fichero como argumento,
     *luego el contenido del vector se escribe en dicho fichero
     * @param a_file
     */
    public void write(String a_file)
    {
        try
        {
            FileWriter t_fw = new FileWriter(a_file);
            PrintWriter t_pw = new PrintWriter(t_fw);
            for (int i = 0; i < m_vector.length; i++)
            {
                t_pw.print(m_vector[i]+" ");
            }
            t_pw.close();
            t_fw.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    /**
     * M�todo que recibe como argumento el nombre de un fichero 
     * y lee el contenido de dicho fichero, el objeto que llame a este
     * m�todo se le asignaran los nuevos datos leidos del fichero
     * @param a_nomFichero
     */
	public void read(String a_nomFichero)
	{
		try
		{
			FileReader t_fReader = new FileReader(new File(a_nomFichero));
			BufferedReader t_bReader = new BufferedReader(t_fReader);
			String aux = "";
			aux = t_bReader.readLine();
			m_vector = converStringToArrayInt(aux.split(" "));
			m_dimesion = m_vector.length;
			t_bReader.close();
			t_fReader.close();
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (NumberFormatException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
    /**
     * M�todo privado que recibe un array de strings para convertirlo
     * en un array de enteros
     * @param a_array
     * @return el nuevo array de enteros
     */
    private int [] converStringToArrayInt(String [] a_array)
	{
		int [] t_resul = new int [a_array.length];
		
		for (int i = 0; i < t_resul.length; i++)
		{
			t_resul[i] = Integer.parseInt(a_array[i]);
		}
		return t_resul;
	}
    /**
     * M�do para a�adir un elemento en una pisici�n dada
     * @param a_elemento
     * @param a_pos
     */
    public void anadir(int a_elemento, int a_pos)
    {
        if (a_pos < m_dimesion && a_pos >=0)
        {
            m_vector[a_pos] = a_elemento;
            if (m_posicion == a_pos)
                m_posicion++;
        } else
			try
			{
				throw new DimesionException("La posici�n a la que se intenta acceder esta fuera del rango");
			} catch (DimesionException e)
			{
				e.printStackTrace();
			}
    }
    /**
     * M�todo que a�ade el elemento pasado como argumento al final del array
     * @param a_elemento
     */
    public void anadir(int a_elemento)
    {
        m_vector[m_posicion] = a_elemento;
        m_posicion++;
    }
    /**
     * M�todo que hace el producto de dos vectores
     * @param a_vector
     * @return
     * @throws DimesionException
     */
	public VectorSinHerencia producto(VectorSinHerencia a_vector) throws DimesionException
	{
		int[] t_vectorAux;
		VectorSinHerencia t_resul = null;
		if (m_dimesion == a_vector.m_dimesion)
		{
			t_vectorAux = new int[m_dimesion];
			for (int i = 0; i < m_vector.length; i++)
			{
				t_vectorAux[i] = m_vector[i] * a_vector.m_vector[i];
			}
			t_resul = new VectorSinHerencia(t_vectorAux, m_dimesion);
		} else
			throw new DimesionException("Vectores con distinta dimesi�n");
		return t_resul;
	}
	/**
	 * M�todo que hace el producto escalar entre 2 vectores
	 * @param a_vector
	 * @return retorna la suma de todos los productos de los vectores
	 * @throws DimesionException
	 */
	public int productoEscalar(VectorSinHerencia a_vector) throws DimesionException
	{
	    VectorSinHerencia t_auxVec = this.producto(a_vector);
	    int t_suma=0;
	    for(int i =0; i<t_auxVec.getM_dimesion();i++)
	    {
	        t_suma += t_auxVec.getDato(i);
	    }
	    return t_suma;
	}
	/**
	 * M�todo para obtener el tama�o del vector
	 * @return
	 */
    public int getM_dimesion()
    {
        return m_dimesion;
    }
    /**
     * M�todo que devuelte el dato que se encuentra en la posici�n pasada 
     * como argumento
     * @param a_pos
     * @return
     */
    public int getDato(int a_pos)
    {
        return m_vector[a_pos];
    }
    @Override
    public String toString()
    {
        String t_resul = "[";
        for (int i = 0; i < m_vector.length; i++)
        {
            t_resul += m_vector[i] + " ";
        }
        t_resul = t_resul + "]";
        return t_resul;
    }
    @Override
	public boolean equals(Object a_obj)
	{
    	VectorSinHerencia t_obj= (VectorSinHerencia) a_obj;
    	if(m_dimesion == t_obj.getM_dimesion())
    	{
	    	for (int i = 0; i < m_vector.length; i++)
			{
				if(m_vector[i] != t_obj.m_vector[i])
					return false;
			} 
	    	return true;
    	}
    	else
    		return false;
	}
}
