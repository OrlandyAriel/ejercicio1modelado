package ull.modelado.ejercicio;

import java.util.ArrayList;
/**
 * @author Orlandy Ariel S�nchez A.
 */
public class Primos extends SerieNumerica
{
	private int m_intvSuperior;
	public Primos()
	{
		this(100);
	}
	public Primos(int a_intvSuperior)
	{
        compruebaError(a_intvSuperior);
        m_intvSuperior = a_intvSuperior;
        m_arraySerie = new ArrayList<Integer>();
        secuencia(0);
	}
	@Override
	public void secuencia(int a_numero)
	{
        m_arraySerie.add(1);
        m_arraySerie.add(2);
        for (int i = 3; i <= m_intvSuperior; i++)
        {
            if (calculoSecuencia(i) == 0)
                m_arraySerie.add(i);
        }
	}
	@Override
	protected int calculoSecuencia(int a_numero)
	{
		int t_salida = 0;
        for (int i = 2; (i <= a_numero && t_salida == 0); i++)
        {
            if(((a_numero % i) == 0) && (a_numero != i))
            {
                t_salida = 1;
                i = m_intvSuperior+1;
            }
        }
		return t_salida;
	}
}
