package ull.modelado.ejercicio;

import ull.modelado.excepciones.DimesionException;

public class Vector extends Matrix
{
    private int m_dimesion;
    private int m_posicion;
    
    public Vector()
    {
        this(1);
    }
    public Vector(int a_dimension)
    {
        super(1,a_dimension);
        m_posicion = 0;
        m_dimesion = a_dimension;
    }
    public Vector(int[][]a_vect,int a_dimension)
    {
       super(1, a_dimension, a_vect);
       m_dimesion = a_dimension;
    }
    public void anadir(int a_elemento, int a_pos)
    {
        anadir(0, a_pos, a_elemento);
    }
    public void anadir(int a_elemento)
    {
        anadir(0, m_posicion, a_elemento);
        m_posicion++;
    }
    public static Vector read(String a_nombre)
    {
        Vector aux = new Vector( 1);
        aux.setM_matrix(Matrix.read(a_nombre));
        return aux;
    }
    public int productoEscalar(Vector a_vec)
    {
        Matrix t_mat = new Matrix(a_vec.m_dimesion, 1);
        for (int i = 0; i < a_vec.m_dimesion; i++)
        {
            t_mat.anadir(i, 0,a_vec.getElemento(0, i));
        }
        try
        {
            t_mat = producto(t_mat);
        } catch (DimesionException e)
        {
            e.printStackTrace();
        }
            
        return t_mat.getElemento(0, 0);
    }
    public int getM_dimesion()
    {
        return m_dimesion;
    }
    @Override
    public String toString()
    {
        String resul= "[ ";
        for (int i = 0; i < getM_matrix().length; i++)
        {
            for (int j = 0; j < getM_matrix()[i].length; j++)
            {
                resul+=getElemento(i, j)+" ";
            }
        }
        resul+="]";
        return resul;
    }
}
