package ull.modelado.ejercicio;

import java.util.ArrayList;
import ull.modelado.excepciones.SecuenciaException;
/**
 * @author Orlandy Ariel S�nchez A.
 */
public class SerieFibonacci extends SerieNumerica
{	// Atributos
	// Constructor/es
	public SerieFibonacci(int a_numero)
	{
		if(compruebaError(a_numero))
		{
			m_arraySerie = new ArrayList<Integer>();
			secuencia(0, a_numero);
		}
	}
	// Funciones
	/**
	 * Determina si un n�mero dado pertenece a la serie fibonacci
	 * @param a_numero: n�mero que se desea saber si pertenece o no
	 * @return si lo es retorna true, sino false
	 */
	public boolean perteceSecuencia(int a_numero)
	{
		if(compruebaError(a_numero))
		{
			int t_aux = 0;
			for (int i = 0; (i <= a_numero + 1 && t_aux < a_numero); i++)
			{
				t_aux= calculoSecuencia(i);
				if(t_aux == a_numero)
					return true;
			}
		}
		return false;
	}
	/**
	 * Func�on para saber, dado un n�mero, si este pertence o no a la serie fibonacci
	 * @param a_numero
	 * @return
	 */
	@Override
	public void secuencia(int a_numero)
	{
		if(compruebaError(a_numero))
		{
			secuencia(0, a_numero);
		}
	}
	/**
	 * Metodo que a�ade y calcula la serie fibonacci dentro de un rango
	 * @param a_n1 limite inferior
	 * @param a_n2 limite superior
	 */
	public void secuencia(int a_n1, int a_n2)
	{
		if(a_n1 < a_n2 )
		{
			m_arraySerie.clear();// reutilizo y para ello lo limpio
			for (int i = 1; i <= a_n2; i++)
			{
				if (i >= a_n1)
					m_arraySerie.add(calculoSecuencia(i));
			}
		} else
			try
			{
				throw new SecuenciaException("El intervalo inferior es mayor que el superior");
			} catch (SecuenciaException e)
			{
				e.printStackTrace();
			}
	}
	@Override
	protected int calculoSecuencia(int a_numero)
	{
		int t_ant = 0;
		int t_fibonacci = 1;
		for (int i = 1; i < a_numero; i++)
		{
			int t_aux = t_ant;
			t_ant = t_fibonacci;
			t_fibonacci = t_aux + t_fibonacci;
		}
		return t_fibonacci;
	}
}
