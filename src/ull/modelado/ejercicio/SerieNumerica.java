package ull.modelado.ejercicio;

import java.util.ArrayList;

import ull.modelado.excepciones.SecuenciaException;

public abstract class SerieNumerica
{ // Atributos
	protected ArrayList<Integer> m_arraySerie;
	// M�todos abstractos
	public abstract void secuencia(int a_numero);
	protected abstract int calculoSecuencia(int a_numero);
	// M�todos implementados
	/**
	 * M�todo que comprueba si el n�mero pasado como argumento
	 * es menor que 1
	 * @param a_num
	 * @return en caso de que sea menor que 1 lanzar� una exepci�n y no continuar� la ejecuci�n
	 */
	protected boolean compruebaError(int a_num)
	{
		if (a_num < 1)
		{
			try
			{
				throw new SecuenciaException("Los n�meros Primos deben ser naturales mayor que 1");
			} catch (SecuenciaException e)
			{	
				
				e.printStackTrace();
				return false;
			}
		}else
			return true;
	}
	/**
	 * M�todo que suma todos n�meros almacenados en el array
	 * @return retorna el resultado de la suma
	 */
	public int suma()
    {
        int t_result=0;
        for (Integer t_num : m_arraySerie)
        {
            t_result+=t_num;
        }
        return t_result;
    }
	@Override
	public String toString()
	{
		String t_resul = "";
		for (int i = 0; i < m_arraySerie.size(); i++)
		{
			t_resul += " " + m_arraySerie.get(i);
		}
		return t_resul;
	}
}
